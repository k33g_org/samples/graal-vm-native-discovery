$JAVA_HOME/bin/native-image \
  --report-unsupported-elements-at-runtime  \
  --enable-all-security-services \
  -jar target/sandbox-1.0.0-SNAPSHOT-jar-with-dependencies.jar \
  releases/sandbox-1.0.0-linux --no-server --static

ls -sh releases/sandbox-1.0.0-linux 