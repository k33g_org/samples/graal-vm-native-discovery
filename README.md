# graal-vm-native-discovery

## Build (fat) jar

```
mvn clean package
```

Run it:

```
java -jar target/sandbox-1.0.0-SNAPSHOT-jar-with-dependencies.jar 
```

## Build executable

```
$JAVA_HOME/bin/native-image \
--report-unsupported-elements-at-runtime  \
--enable-all-security-services \
-jar target/sandbox-1.0.0-SNAPSHOT-jar-with-dependencies.jar \
releases/sandbox-1.0.0-linux --no-server --static
```

## Run it

```
releases/sandbox-1.0.0-linux @k33g
```